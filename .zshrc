BASE16_SHELL=$HOME/.config/base16-shell/
[ -n "$PS1" ] && [ -s $BASE16_SHELL/profile_helper.sh ] && eval "$($BASE16_SHELL/profile_helper.sh)"
source ~/antigen.zsh

# Load the oh-my-zsh library
antigen use oh-my-zsh

# Syntax highlighting bundle
antigen bundle zsh-users/zsh-syntax-highlighting

# Autocomplete
antigen bundle zsh-users/zsh-autosuggestions

# Powerline
antigen bundle

# Load the theme
antigen theme https://github.com/denysdovhan/spaceship-zsh-theme spaceship

# Finalize
antigen apply

# Required for fasd to load correctly
eval "$(fasd --init auto)"
alias j='vim `sf`'
source $ZSH/plugins/fasd/fasd.plugin.zsh
