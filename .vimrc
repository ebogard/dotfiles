syntax on

" Remove trailing whitespace
nnoremap <silent> <F5> :let _s=@/ <Bar> :%s/\s\+$//e <Bar> :let @/=_s <Bar> :nohl <Bar> :unlet _s <CR>

" Tabs and spaces
set tabstop=4
set expandtab
set softtabstop=4
set shiftwidth=4
filetype indent on

set termguicolors
set number

"Plugins
call plug#begin()
Plug 'https://github.com/Valloric/YouCompleteMe.git'
Plug 'https://github.com/tpope/vim-commentary'
Plug 'https://github.com/altercation/vim-colors-solarized'
Plug 'https://github.com/joshdick/onedark.vim'
Plug 'nightsense/vim-crunchbang'
Plug 'https://github.com/sheerun/vim-polyglot'
Plug 'flazz/vim-colorschemes'
Plug 'bronson/vim-trailing-whitespace'
Plug 'chriskempson/base16-vim'
Plug 'altercation/vim-colors-solarized'
Plug 'valloric/matchtagalways'
call plug#end()

" Theme
if filereadable(expand("~/.vimrc_background"))
  let base16colorspace=256
  source ~/.vimrc_background
endif
