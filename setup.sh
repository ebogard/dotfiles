# Copy files to home directory
rsync -av --exclude=.git --exclude=setup.sh --exclude=one_dark.itermcolors . ~

if [[ "$OSTYPE" == "darwin"* ]]; then
    # Mac OSX
    
    which -s brew
    if [[ $? != 0 ]]; then    
	echo "Installing homebrew..."
	/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    fi

    echo "Installing zsh..."
    brew install zsh

    echo "Installing fasd..."
    brew install fasd

    echo "Installing tree..."
    brew install tree
else
	# Probly Linux
	sudo yum upgrade
	sudo yum install zsh

	sudo add-apt-repository ppa:aacebedo/fasd
	sudo apt-get update
	sudo apt-get install zsh
	sudo apt-get install fasd
	sudo apt-get install tree
	sudo apt-get install curl
fi

# Vim plugin manager
echo "Installing vim-plug..."
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
        https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# Set terminal to zsh
chsh -s $(which zsh)
